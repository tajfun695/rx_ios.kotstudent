//
//  Autherntication.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 04/07/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import Foundation
import Alamofire

public class Authentication: Request {
    public static func LoginViaEmail(_username: String, _password: String, completion: @escaping (Result<Status>) -> ()) {
        removeCookies()
        let param: [String: String] = ["emailAddress": _username, "plainPassword": _password]
        baseURL.path = "/auth/connect-email-address/"
        Alamofire.request(baseURL, method: .post, parameters: param, encoding: JSONEncoding.default, headers: requestHeader)
            .responseJSON { (response) in
                switch response.result{
                case .success:
                    let returnData: String = String(data: response.data!, encoding: .utf8)!
                    if(returnData.contains("status")){
                        let header = response.response?.allHeaderFields as? [String:String]
                        Request.setCookies(cookies: HTTPCookie.cookies(withResponseHeaderFields: header!, for: (response.response?.url)!), response: response.response!)
                        completion(Result.onSuccess(Status(status: true)))
                        break
                    }
                    
                    if let json = try? JSONSerialization.jsonObject(with: response.data!, options: []) as! [String : Any]{
                        if let error = json["Error"] as? [String: Any] {
                            completion(.onFail(APIError(_identifier: response.response!.statusCode, _description: error["description"] as? String ?? "")))
                        }
                    }
                case .failure:
                    break
                }
        }
    }
    
    public static func LoginViaFacebook(_token: String, completion: @escaping (Result<Status>) -> ()) {
        removeCookies()
        baseURL.path = "/auth/connect-with-facebook/" + _token
        Alamofire.request(baseURL).responseJSON { (response) in
            switch response.result {
            case .success:
                let header = response.response?.allHeaderFields as? [String:String]
                Request.setCookies(cookies: HTTPCookie.cookies(withResponseHeaderFields: header!, for: (response.response?.url)!), response: response.response!)
                completion(Result.onSuccess(Status(status: true)))
            case .failure:
                break
            }
        }
    }
    
    public static func Logout(completion: @escaping (Result<Status>) -> ()){
        baseURL.path = "/auth/drop-me/"
        Alamofire.request(baseURL, method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: requestHeader)
            .responseJSON { (response) in
                switch response.result{
                case .success:
                    let returnData: String = String(data: response.data!, encoding: .utf8)!
                    if(returnData.contains("status")){
                       let result = try! JSONDecoder().decode(Status.self, from: response.data!)
                       completion(Result.onSuccess(result))
                       break
                    }
                    
                    if let json = try? JSONSerialization.jsonObject(with: response.data!, options: []) as! [String : Any]{
                        if let error = json["Error"] as? [String: Any] {
                            completion(.onFail(APIError(_identifier: response.response!.statusCode, _description: error["description"] as? String ?? "")))
                        }
                    }
                case .failure:
                    break
                }
        }
    }
}
