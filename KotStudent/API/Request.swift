//
//  Request.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 04/07/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import Foundation
import Alamofire

public class Request {
    //static let baseUrl = "http://web-kpawlik.kotek.xara.ovh:8083/api"
    public static var baseURL: URLComponents =  URLComponents(string: "https://api.kotstudent.xara.ovh")!
    //public static let baseURL: String = "https://api.kotstudent.xara.ovh"
    public static let requestHeader: [String : String] = ["Content-Type" : "application/json"]
    
    public static func GetMethod(path:String, query: [URLQueryItem]?, result: @escaping (Data?, Error?) -> ()) {
        baseURL.path = path
        baseURL.queryItems = query
        Alamofire.request(baseURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: requestHeader)
            .validate(statusCode: 200..<300)
            .responseJSON { (response) in
            switch response.result{
            case .success:
                result(response.data!, nil)
                break
            case .failure:
                if let json = try? JSONSerialization.jsonObject(with: response.data!, options: []) as! [String : Any]{
                    if let error = json["Error"] as? [String: Any] {
                        result(nil, APIError(_identifier: response.response!.statusCode, _description: error["description"] as? String ?? ""))
                    }
                }
                break
            }    
        }
    }
    
    public static func OtherMethod(path:String, query: [URLQueryItem]?, method: HTTPMethod, header: HTTPHeaders = requestHeader, data: [String: Any], result: @escaping (Data?, Error?) -> ()){
        baseURL.path = path
        baseURL.queryItems = query
        Alamofire.request(baseURL, method: method, parameters: data, encoding: JSONEncoding.default, headers: header)
            .validate(statusCode: 200..<300)
            .responseJSON { (response) in
            switch response.result {
            case .success:
                result(response.data, nil)
                break
            case .failure:
                if let json = try? JSONSerialization.jsonObject(with: response.data!, options: []) as! [String : Any]{
                    if let error = json["Error"] as? [String: Any] {
                        result(nil, APIError(_identifier: response.response!.statusCode, _description: error["description"] as? String ?? ""))
                    }
                }
                break
            }
        }
    }
    
    static func setCookies(cookies: [HTTPCookie], response: HTTPURLResponse){
        Alamofire.SessionManager.default.session.configuration.httpCookieStorage?.setCookies(cookies, for: response.url!, mainDocumentURL: nil)
    }
    
    static func removeCookies(){
        let cstorage = HTTPCookieStorage.shared
        if let cookies = cstorage.cookies {
            for cookie in cookies {
                cstorage.deleteCookie(cookie)
            }
        }
    }
    
    static func isCookies() -> Bool{
        let cstorage = HTTPCookieStorage.shared.cookies(for: URL(string: baseURL.description)!)
        
        if(cstorage?.isEmpty)! {
            return true
        }
        return false
    }
    
}

