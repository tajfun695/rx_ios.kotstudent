//
//  GroupAPI.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 06/07/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import Foundation
import Alamofire

public class GroupAPI: Request {
    public static func GetGroupChilds(groupId: Int, offset: Int? = nil, completion: @escaping (Result<[Group]>) -> ()) {
        let queryItems: [URLQueryItem] = [URLQueryItem(name: "groupId", value: groupId.description), URLQueryItem(name: "offset", value: offset?.description)]
        GetMethod(path: "/group/child", query: queryItems) { (result, error) in
            if error != nil {
                guard let er = error else { return }
                completion(.onFail(er))
                return
            }
            
            var groups: [Group] = []
            guard let rawJson = result else { completion(.onSuccess([])); return }
            if let json = try? JSONSerialization.jsonObject(with: rawJson, options: []) as! [String: Any] {
                print(json)
                for item in json["GroupChildList"] as! [[String: Any]] {
                    if let group = try? Group(json: item) {
                        groups.append(group)
                    }
                }
            }
            completion(.onSuccess(groups))
            
        }
    }
    
    public static func CreateCollege() {
    }
    
    public static func FindCollege(collegeNamePhrase: String, completion: @escaping (Result<[Group]>) -> ()) {
        let queryItems: [URLQueryItem] = [URLQueryItem(name: "collegeNamePhrase", value: collegeNamePhrase)]
        GetMethod(path: "/group/college/find", query: queryItems) { (result, error) in
            if error != nil {
                guard let er = error else { return }
                completion(.onFail(er))
                return
            }
            
            var groups: [Group] = []
            guard let rawJson = result else { completion(.onSuccess([])); return }
            if let json = try? JSONSerialization.jsonObject(with: rawJson, options: []) as! [String: Any] {
                print(json)
                for item in json["Colleges"] as! [[String: Any]] {
                    if let group = try? Group(json: item) {
                        groups.append(group)
                    }
                }
            }
            completion(.onSuccess(groups))
        }
    }
    
    public static func FindGroups(searchPhrase: String, startFrom: Int? = nil, completion: @escaping (Result<[Group]>) -> ()) {
        let queryItems: [URLQueryItem] = [URLQueryItem(name: "searchPhrase", value: searchPhrase), URLQueryItem(name: "startFrom", value: startFrom?.description)]
        GetMethod(path: "/group/find/group-by/first-letter", query: queryItems) { (result, error) in
            if error != nil {
                if let er = error {
                    completion(.onFail(er))
                    return
                }
            }
                
            var groups: [Group] = []
            guard let rawJson = result else { completion(.onSuccess([])); return }
            if let json = try? JSONSerialization.jsonObject(with: rawJson, options: []) as! [String: Any] {
                for lettter in json["Groups"] as! [String: Any] {
                    for item in lettter.value as! [Any] {
                        if let group = try? Group(json: item as! [String: Any]) {
                            groups.append(group)
                        }
                    }
                }
            }
            completion(.onSuccess(groups))  
        }
    }
    
    public static func GetCompleteInfo(groupId: Int, completion: @escaping (Result<ComplexGroup>) -> ()) {
        GetMethod(path: "/group/\(groupId)/complex", query: nil) { (result, error) in
            if error != nil {
                if let er = error {
                    completion(.onFail(er))
                    return
                }
            }
            
            guard let rawJson = result else { return }
            if let json = try? JSONSerialization.jsonObject(with: rawJson, options: []) as! [String: Any] {
                let item = json["GroupComplexInfo"] as! [String: Any]
                completion(.onSuccess(ComplexGroup(group: item["group"] as! [String: Any], topUsers: item["topMembers"] as! [Any], threads: item["threads"] as! [Any])))
            }
        }
    }
    
    public static func GetMyGroups(limit: Int? = nil, offset: Int? = nil, completion: @escaping (Result<[Group]>) -> ()) {
        let queryItems: [URLQueryItem] = [URLQueryItem(name: "limit", value: limit?.description), URLQueryItem(name: "offset", value: offset?.description)]
        GetMethod(path: "/group/my-groups", query: queryItems) { (result, error) in
            if error != nil {
                if let er = error {
                    completion(.onFail(er))
                    return
                }
            }
            
            var groups: [Group] = []
            guard let rawJson = result else { completion(.onSuccess([])); return }
            if(!String(data: rawJson, encoding: .utf8)!.contains("Error")){
                if let json = try? JSONSerialization.jsonObject(with: rawJson, options: []) as! [String: Any] {
                    for item in json["UserGroups"] as! [[String: Any]] {
                        if let group = try? Group(json: item) {
                            groups.append(group)
                        }
                    }
                }
            }
            completion(.onSuccess(groups))
        }
    }
    
    public static func MostSerchead(limit: Int?, groupType: Int?, completion: @escaping (Result<[Group]>) -> ()) {
        let queryItems: [URLQueryItem] = [URLQueryItem(name: "limit", value: limit?.description), URLQueryItem(name: "group_type", value: groupType?.description)]
        GetMethod(path: "/group/most-search", query: queryItems) { (result, error) in
            if error != nil {
                if let er = error {
                    completion(.onFail(er))
                    return
                }
            }
            
            var groups: [Group] = []
            guard let rawJson = result else { completion(.onSuccess([])); return }
            if let json = try? JSONSerialization.jsonObject(with: rawJson, options: []) as! [String: Any] {
                for lettter in json["OftenSearch"] as! [String: Any] {
                    for item in lettter.value as! [Any] {
                        if let group = try? Group(json: item as! [String: Any]) {
                            groups.append(group)
                        }
                    }
                }
            }
            completion(.onSuccess(groups))
        }
    }
    
    public static func GetPosts(threadId: Int, lastPost: Int? = nil, completion: @escaping (Result<[Post]>) -> ()) {
        let queryItems: [URLQueryItem] = [URLQueryItem(name: "limit", value: lastPost?.description)]
        GetMethod(path: "/group/threads/\(threadId)/posts", query: queryItems) { (result, error) in
            if error != nil {
                if let er = error {
                    completion(.onFail(er))
                    return
                }
            }
            
            var posts: [Post] = []
            guard let rawJson = result else { completion(.onSuccess([])); return }
            if let json = try? JSONSerialization.jsonObject(with: rawJson, options: []) as! [String: Any] {
                print(json)
                for item in json["ThreadPosts"] as! [[String: Any]] {
                    if let post = try? Post(json: item) {
                        posts.append(post)
                    }
                }
            }
            print("\(posts.count)  🍒🍒🍒🍒🍒🍒🍒🍒🍒🍒🍒🍒🍒🍒🍒🍒🍒🍒")
            completion(.onSuccess(posts))
        }
    }
}
