//
//  AppDelegate.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 04/07/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    private var viewModel: AppDelegateViewModel = AppDelegateViewModel()
    private var disposeBag: DisposeBag = DisposeBag()
    private var reachability:Reachability!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        Language.loadLanguage(NSLocale.preferredLanguages[0])
        
        window = UIWindow(frame: UIScreen.main.bounds)
        self.setupNetworkStatus()
        self.setupViewModel()
        UINavigationBar.setupNavigationBar()

        return true
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        
        return handled
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    fileprivate func setupNetworkStatus() {
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: ReachabilityChangedNotification, object: nil)
        self.reachability = Reachability.init()
        do { try self.reachability.startNotifier() }
        catch {}
    }
    
    fileprivate func setupViewModel() {
        self.viewModel.isLogged.subscribe(onNext: { isLogged in
            self.window?.rootViewController = isLogged ? MenuBarView() : LoginView()
            self.window?.makeKeyAndVisible()
        }).disposed(by: disposeBag)
        
    }
    
    //MARK:- Network Check
    @objc func reachabilityChanged(notification:Notification) {
        let reachability = notification.object as! Reachability
        if reachability.isReachable {
            if reachability.isReachableViaWiFi {
                print("Reachable via WiFi")
                CheckNetwork.Status.value = CheckNetwork.StatusNetwork.WiFi
            } else {
                print("Reachable via Cellular")
                CheckNetwork.Status.value = CheckNetwork.StatusNetwork.LTE
            }
        } else {
            print("Network not reachable")
            CheckNetwork.Status.value = CheckNetwork.StatusNetwork.NoConnect
        }
    }
}

