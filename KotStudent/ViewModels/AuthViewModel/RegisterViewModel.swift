//
//  RegisterViewModel.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 23/07/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import Foundation
import RxSwift

class RegisterViewModel {
    var email: Variable<String> = Variable<String>("")
    var username: Variable<String> = Variable<String>("")
    var password: Variable<String> = Variable<String>("")
    
    var isValid: Observable<Bool> {
        return Observable.combineLatest(email.asObservable(), username.asObservable(), password.asObservable()) { email, username, password in
            EmailValidation.isValidEmail(testStr: email) && username.count > 6 && password.count > 6
        }
    }
    
    func RegisterUser() -> Observable<Bool> {
        return Observable.create { observer in
            return Disposables.create()
        }
    }
}
