//
//  LoginViewModel.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 23/07/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import UIKit
import Foundation
import RxSwift

class LoginViewModel {
    var emailTextField = Variable<String>("")
    var passwordTextField = Variable<String>("")
    
    var isValid: Observable<Bool> {
        return Observable.combineLatest(emailTextField.asObservable(), passwordTextField.asObservable(), CheckNetwork.Status.asObservable()) {
            email, password, network in
            if(email.count >= 3 && password.count >= 3 && network != CheckNetwork.StatusNetwork.NoConnect) {
                return true
            }
            return false
        }
    }
    
    var isNetwork: Observable<Bool> {
        return Observable.combineLatest(emailTextField.asObservable(), CheckNetwork.Status.asObservable()) { x, network in
            if(network != CheckNetwork.StatusNetwork.NoConnect) {
                return true
            }
            return false
        }
    }
    
    func LoginViaEmail() -> Observable<Bool>  {
        return Observable.create { observer in
            Authentication.LoginViaEmail(_username: self.emailTextField.value, _password: self.passwordTextField.value) { status in
                switch status {
                case .onSuccess:
                    observer.onNext(true)
                case .onFail(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    
    func LoginViaFacebook(token: String) -> Observable<Bool> {
        return Observable.create { observer in
            Authentication.LoginViaFacebook(_token: token) { status in
                switch status {
                case .onSuccess:
                    observer.onNext(true)
                case .onFail(let error):
                    observer.onError(error)
                }
                observer.onCompleted()
            }
            return Disposables.create()
        }
    }
}
