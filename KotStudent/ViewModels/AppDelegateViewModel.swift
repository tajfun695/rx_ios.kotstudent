//
//  AppDelegateViewModel.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 13/08/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import Foundation
import RxSwift

class AppDelegateViewModel {
    
    var isLogged: Observable<Bool> {
        return Observable.create{ observer -> Disposable in
            if Request.isCookies() {
                observer.onNext(true)
            }
            observer.onNext(false)
            return Disposables.create()
        }
    }
}
