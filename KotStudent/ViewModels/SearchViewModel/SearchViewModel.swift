//
//  SearchViewModel.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 13/08/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import Foundation
import RxSwift

class SearchViewModel {
    //var searchedGroup: Variable<[Group]> = Variable<[Group]>([])
    
    func loadGroups(searchText: String) -> Observable<[Group]> {
        return Observable.create{ observer -> Disposable in
            GroupAPI.FindGroups(searchPhrase: searchText) { (status) in
                switch status {
                case .onSuccess(let groups):
                    observer.onNext(groups)
                case .onFail(_):
                    break
                }
            }
            return Disposables.create()
        }
    }
}
