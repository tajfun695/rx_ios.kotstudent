//
//  SingielGroupViewModel.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 20/08/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import Foundation
import RxSwift
import RxDataSources

class SingielGroupViewModel {
    var groupID: Int = 0;
    var group: Variable<ComplexGroup> = Variable(ComplexGroup())
    var posts: Variable<[Post]> = Variable([])
    
    let dataSource = RxCollectionViewSectionedReloadDataSource<SectionViewModel>(configureCell: { _,_,_,_ in
        fatalError()
    },
    configureSupplementaryView: { _,_,_,_ in
        fatalError()
    })
    
    var isValid: Observable<Bool> {
        return Observable.combineLatest(group.asObservable(), posts.asObservable(), CheckNetwork.Status.asObservable()) {
            group, posts, network in
            if(group.threads.count > 0 && posts.count > 0 && network != CheckNetwork.StatusNetwork.NoConnect) {
                return true
            }
            return false
        }
    }
    
    init(groupID: Int){
        self.groupID = groupID
    }
    
    func LoadGroup(completion: @escaping (Bool) -> ()){
        GroupAPI.GetCompleteInfo(groupId: groupID) { status in
            switch status {
            case .onSuccess(let group):
                self.group.value = group
                completion(true)
            case .onFail(_):
                break
            }
        }
    }
    
    func LoadPosts(threadId: Int? = nil, lastPostId: Int? = nil, completion: @escaping (Bool) -> ()) {
        GroupAPI.GetPosts(threadId: threadId == nil ? group.value.threads[0].id! : threadId!, lastPost: lastPostId) { status in
            switch status {
            case .onSuccess(let posts):
                self.posts.value = posts
                completion(true)
            case .onFail(_):
                break
            }
        }
    }
}
