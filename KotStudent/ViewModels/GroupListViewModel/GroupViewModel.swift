//
//  GroupView.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 23/07/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import Foundation
import RxSwift

class GroupViewModel {
    var groups: Variable<[Group]> = Variable<[Group]>([])
    
    func loadGroups() {
        GroupAPI.GetMyGroups { (status) in
            switch status {
            case .onSuccess(let groups):
                self.groups.value = groups
                break
            case .onFail(_):
                break
            }
        }
    }
}
