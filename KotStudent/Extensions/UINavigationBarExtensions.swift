//
//  UINavigationBarExtensions.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 20/08/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationBar {
    static func setupNavigationBar() {
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().barTintColor = Color.mainColor
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        UINavigationBar.appearance().isTranslucent = false
    }
}
