//
//  DataConverter.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 13/07/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import Foundation

class DataConverter {
    static func DateParse(date: String?) -> Date? {
        if(date != nil){
            let dateFor: DateFormatter = DateFormatter()
            dateFor.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            
            return dateFor.date(from: date!)
        }
        return nil
    }
}
