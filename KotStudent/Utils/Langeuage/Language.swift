//
//  Language.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 03.03.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit

class Language {
    public static var Dictionary: [String: String] = [:]
    static func loadLanguage(_ key: String) {
        switch key {
        case "pl-PL":
            Dictionary = PolishLaguage().getLanguage()
            break
        case "en-EN":
            Dictionary = EnglishLanguage().getLanguage()
            break
        default:
            Dictionary = EnglishLanguage().getLanguage()
            break
        }
    }
    
    static func getWord(_ key: String) -> String! {
        return Dictionary[key]
    }
}
