//
//  PolishLanguage.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 03.03.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

class PolishLaguage {
    func getLanguage() -> [String: String]{
        var dictionary: [String: String] = [:]
        
        dictionary["Sign In"] = "Zaloguj się"
        dictionary["Sign Up"] = "Zarejstruj się"
        dictionary["Don`t have any account?"] = "Nie masz konta?"
        dictionary["Create One"] = "Zarejstruj się!"
        dictionary["E-mail"] = "E-mail"
        dictionary["Password"] = "Hasło"
        dictionary["UserName"] = "Nazwa użytkownika"
        dictionary["HaveAccount"] = "Mam już konto!"
        
        return dictionary
    }
}
