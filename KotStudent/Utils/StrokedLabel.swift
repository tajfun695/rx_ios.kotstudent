//
//  UILabel+Extansions.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 03/09/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class StrokedLabel: UILabel {
    let strokeTextAttributes = [
        NSAttributedStringKey.strokeColor.rawValue : UIColor.black,
        NSAttributedStringKey.foregroundColor : UIColor.white,
        NSAttributedStringKey.strokeWidth : -4.0,
        NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 30)
        ] as! [NSAttributedStringKey : Any]
}

extension Reactive where Base: UILabel {
    /// Bindable sink for `text` property.
    internal var strockText: Binder<StrokedLabel> {
        return Binder(base){ label, text in
            label.attributedText = NSMutableAttributedString(string: text.text!, attributes: text.strokeTextAttributes)
        }
    }
}

