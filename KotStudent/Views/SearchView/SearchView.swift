//
//  SearchView.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 03/08/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class SearchView: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    //MARK: Variable
    let searchTextBox: UITextField = UITextField()
    let viewModel: SearchViewModel = SearchViewModel()
    private let cellID: String = "SearchGroupCellID"
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigator()
        self.setupBody()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    fileprivate func setupNavigator() {
        let cancelTap = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(dismissView))
        
        self.navigationItem.rightBarButtonItems = [cancelTap]
        self.navigationItem.leftBarButtonItems = [cancelTap]
        
        searchTextBox.placeholder = "Search"
        searchTextBox.backgroundColor = Color.lightGrey
        searchTextBox.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.frame.width, height: 21)
        self.navigationItem.titleView = searchTextBox
    }
    
    fileprivate func setupBody() {
        self.collectionView?.register(GroupsViewCell.self, forCellWithReuseIdentifier: cellID)
        collectionView?.backgroundColor = Color.lightGrey
        self.collectionView?.dataSource = nil
        
        let searchResults = searchTextBox.rx.text.orEmpty.throttle(0.3, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .flatMap { query -> Observable<[Group]> in
            if query.isEmpty && query.count <= 3 {
                return .just([])
            } else {
                return self.viewModel.loadGroups(searchText: query)
                    .catchErrorJustReturn([])
            }
        }.observeOn(MainScheduler.instance)

        
        searchResults.bind(to: collectionView!.rx.items(cellIdentifier: cellID, cellType: GroupsViewCell.self))
        { index, item, cell in
            cell.data = item
        }.disposed(by: disposeBag)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    @objc func dismissView() {
        self.navigationController?.popViewController(animated: false)
    }
}
