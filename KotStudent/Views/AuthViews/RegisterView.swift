//
//  RegisterAccountView.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 04.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import SnapKit
import UIKit
import RxCocoa
import RxSwift

class RegisterView: UIViewController {
    
    var viewModel = RegisterViewModel()
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupConstraints()
        self.hideKeyboardWhenTappedAround()
        
        _ = emailField.rx.text.map { $0 ?? ""}.bind(to: viewModel.email)
        _ = nameField.rx.text.map { $0 ?? ""}.bind(to: viewModel.username)
        _ = passwordField.rx.text.map { $0 ?? ""}.bind(to: viewModel.password)
        _ = viewModel.isValid.bind(to: registerButton.rx.isEnabled)
        
        viewModel.isValid.subscribe(onNext: { isValid in
            self.registerButton.isEnabled = isValid ? true : false
            self.registerButton.backgroundColor = isValid ? Color.secondaryColor : UIColor.gray
        }).disposed(by: disposeBag)
    }
    
    fileprivate func setupUI(){
        view.backgroundColor = Color.mainColor
        view.addSubview(logo)
        view.addSubview(emailField)
        view.addSubview(nameField)
        view.addSubview(passwordField)
        view.addSubview(registerButton)
        view.addSubview(haveAccLabel)
    }
    
    fileprivate func setupConstraints(){
        logo.snp.makeConstraints { (make) in
            make.width.height.equalTo(150)
            make.centerX.equalTo(view.center)
            make.topMargin.equalTo(view.frame.width / 3)
        }
        
        emailField.snp.makeConstraints { (make) in
            make.top.equalTo(logo.snp.bottom).offset(24)
            make.left.equalTo(view.snp.left).offset(24)
            make.right.equalTo(view.snp.right).offset(-24)
            make.height.equalTo(50)
        }
        
        nameField.snp.makeConstraints { (make) in
            make.top.equalTo(emailField.snp.bottom).offset(0.5)
            make.left.right.equalTo(emailField)
            make.height.equalTo(emailField)
        }
        
        passwordField.snp.makeConstraints { (make) in
            make.top.equalTo(nameField.snp.bottom).offset(0.5)
            make.left.right.equalTo(emailField)
            make.height.equalTo(emailField)
        }
        
        registerButton.snp.makeConstraints { (make) in
            make.top.equalTo(passwordField.snp.bottom).offset(12)
            make.left.right.equalTo(emailField)
            make.height.equalTo(emailField)
        }
        
        haveAccLabel.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-16)
            make.centerX.equalToSuperview()
        }
    }
    
    @objc fileprivate func handledLogin(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @objc fileprivate func handledRegister() {
        registerButton.press()
        
    }
    
    var logo: UIImageView = {
        let view = UIImageView(image: UIImage(named: "Logo"))
        view.frame = view.frame
        view.contentMode = .scaleToFill
        return view
    }()
    
    var emailField: UITextField = {
        let textView = UITextField()
        textView.placeholder = Language.getWord("E-mail")
        textView.autocorrectionType = .no
        textView.backgroundColor = .white
        textView.keyboardType = UIKeyboardType.emailAddress
        textView.autocapitalizationType = .none
        textView.setLeftPaddingPoints(8)
        return textView
    }()
    
    var passwordField: UITextField = {
        let textView = UITextField()
        textView.placeholder = Language.getWord("Password")
        textView.backgroundColor = .white
        textView.setLeftPaddingPoints(8)
        textView.isSecureTextEntry = true;
        return textView
    }()
    
    lazy var loginButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = Color.secondaryColor
        button.setTitle(Language.getWord("Sign In"), for: .normal)
        button.addTarget(self, action: #selector(handledLogin), for: .touchUpInside)
        return button
    }()
    
    var registerLabel: UILabel = {
        var label = UILabel()
        label.textColor = .white
        label.font = label.font.withSize(12)
        label.text = Language.getWord("Don`t have any account?")
        return label
    }()
    
    lazy var createOneLabel: UILabel = {
        var label = UILabel()
        label.textColor = Color.secondaryColor
        label.font = UIFont.boldSystemFont(ofSize: 12.0)
        label.attributedText = NSAttributedString(string: Language.getWord("Create One"), attributes: [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        var tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handledRegister))
        tapGestureRecognizer.numberOfTapsRequired = 1;
        label.addGestureRecognizer(tapGestureRecognizer)
        label.isUserInteractionEnabled = true;
        return label
    }()
    
    var separator: UIView = {
        var view = UIView()
        view.backgroundColor = .gray
        return view;
    }()
    
    var nameField: UITextField = {
        let textView = UITextField()
        textView.placeholder = Language.getWord("UserName")
        textView.autocorrectionType = .no
        textView.backgroundColor = .white
        textView.setLeftPaddingPoints(8)
        return textView
    }()
    
    lazy var registerButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = Color.secondaryColor
        button.setTitle(Language.getWord("Sign Up"), for: .normal)
        button.addTarget(self, action: #selector(handledRegister), for: .touchUpInside)
        return button
    }()
    
    lazy var haveAccLabel: UILabel = {
        var label = UILabel()
        label.textColor = Color.secondaryColor
        label.font = UIFont.boldSystemFont(ofSize: 12.0)
        label.attributedText = NSAttributedString(string: Language.getWord("HaveAccount"), attributes: [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        var tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handledLogin))
        tapGestureRecognizer.numberOfTapsRequired = 1;
        label.addGestureRecognizer(tapGestureRecognizer)
        label.isUserInteractionEnabled = true;
        return label
    }()
}
