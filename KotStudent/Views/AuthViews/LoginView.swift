//
//  LoginView.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 23/07/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import Foundation
import UIKit
import FBSDKLoginKit
import RxSwift
import RxCocoa
import SnapKit

class LoginView: UIViewController, FBSDKLoginButtonDelegate {
    
    //MARK: VARIABLES
    let viewModel: LoginViewModel = LoginViewModel()
    let noConnect: NoConnectView = NoConnectView()
    let spinningView: SpinnerView = SpinnerView()
    let disposedBag = DisposeBag()
    
    //MARK: METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true;
        self.view.backgroundColor = Color.mainColor
        self.hideKeyboardWhenTappedAround()
        self.setupUI()
        self.setupConstraints()
        
        _ = emailField.rx.text.map { $0 ?? "" }.bind(to: viewModel.emailTextField)
        _ = passwordField.rx.text.map { $0 ?? ""}.bind(to: viewModel.passwordTextField)
        _ = viewModel.isValid.bind(to: loginButton.rx.isEnabled)
        
        viewModel.isValid.subscribe(onNext: { isValid in
            self.loginButton.isEnabled = isValid ? true : false
            self.loginButton.backgroundColor = isValid ? Color.secondaryColor : UIColor.gray
        }).disposed(by: disposedBag)
        
        viewModel.isNetwork.subscribe(onNext: { isNetwork in
            self.noConnect.isHidden = isNetwork ?  true : false
            self.fBloginButton.isEnabled = isNetwork ? true : false
        }).disposed(by: disposedBag)
    }
    
    @objc func handledLogin(){
        self.loginButton.press()
        self.spinningView.isHidden = false
        self.loginButton.setTitle("", for: .normal)
        let isLoggedByEmail = viewModel.LoginViaEmail()
        isLoggedByEmail.subscribe (
        onNext: { (logged) in
            self.insertChildController(MenuBarView(), intoParentView: self.view)
            self.dismiss(animated: true, completion: nil)
        },
        onError: { (err) in
            print(err)
            let alertController = UIAlertController(title: "Error", message: err.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
            self.spinningView.isHidden = true;
            self.loginButton.setTitle(Language.getWord("Sign In"), for: .normal)
        }).disposed(by: disposedBag)
    }
    
    @objc func handledRegister() {
        let registerView = RegisterView()
        self.present(registerView, animated: true, completion: nil)
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil || result.isCancelled {
            return
        }
        
        let isLoggedByFacebook = viewModel.LoginViaFacebook(token: result.token.tokenString)
        isLoggedByFacebook.subscribe(
            onNext: { logged in self.dismiss(animated: true, completion: nil)},
            onError: { err in
            print(err)
            let alertController = UIAlertController(title: "Error", message: err.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }).disposed(by: disposedBag)
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
    }
     
    fileprivate func setupUI(){
        view.addSubview(logo)
        view.addSubview(emailField)
        view.addSubview(separator)
        view.addSubview(passwordField)
        view.addSubview(loginButton)
        view.addSubview(fBloginButton)
        view.addSubview(registerLabel)
        view.addSubview(createOneLabel)
        view.addSubview(noConnect)
        view.addSubview(spinningView)
    }
    
    fileprivate func setupConstraints(){
        
        self.spinningView.isHidden = true
        spinningView.snp.makeConstraints{ make in
            make.center.equalTo(loginButton)
            make.width.height.equalTo(32)
        }
        
        noConnect.snp.makeConstraints{ make in
            make.top.equalToSuperview()
            make.left.right.equalToSuperview()
            make.height.equalTo(48)
        }
        
        logo.snp.makeConstraints { (make) in
            make.width.equalTo(150)
            make.height.equalTo(150)
            //make.top.equalTo(noConnect.snp.bottom)
            make.centerX.equalTo(view.center)
            make.top.equalTo(view.frame.width / 3)
        }
        
        emailField.snp.makeConstraints { (make) in
            make.top.equalTo(logo.snp.bottom).offset(24)
            make.left.equalTo(view.snp.left).offset(24)
            make.right.equalTo(view.snp.right).offset(-24)
            make.bottom.equalTo(separator.snp.top)
            make.height.equalTo(50)
        }
        
        separator.snp.makeConstraints { (make) in
            make.top.equalTo(emailField.snp.bottom)
            make.left.right.equalTo(emailField)
            make.bottom.equalTo(passwordField.snp.top)
            make.height.equalTo(1)
        }
        
        passwordField.snp.makeConstraints { (make) in
            make.top.equalTo(separator.snp.bottom)
            make.left.right.equalTo(emailField)
            make.height.equalTo(50)
        }
        
        loginButton.snp.makeConstraints { (make) in
            make.top.equalTo(passwordField.snp.bottom).offset(12)
            make.left.right.equalTo(emailField)
            make.height.equalTo(50)
        }
        
        fBloginButton.snp.makeConstraints { (make) in
            make.top.equalTo(loginButton.snp.bottom).offset(12)
            make.left.right.equalTo(emailField)
            make.height.equalTo(50)
            make.centerX.equalTo(view.center)
        }
        
        registerLabel.snp.makeConstraints { (make) in
            let positionOfText = (view.frame.width - (registerLabel.intrinsicContentSize.width + createOneLabel.intrinsicContentSize.width)) / 2
            make.bottom.equalTo(view.snp.bottom).offset(-16)
            make.leftMargin.equalTo(positionOfText)
        }
        
        createOneLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(view.snp.bottom).offset(-16)
            make.left.equalTo(registerLabel.snp.right).offset(4)
        }
    }
    
    // MARK: Components
    lazy var fBloginButton: FBSDKLoginButton = {
        let loginButton = FBSDKLoginButton()
        loginButton.delegate = self
        return loginButton
    }()
    
    var logo: UIImageView = {
        let view = UIImageView(image: UIImage(named: "Logo"))
        view.frame = view.frame
        view.contentMode = .scaleToFill
        return view
    }()
    
    var emailField: UITextField! = {
        let textView = UITextField()
        textView.placeholder = Language.getWord("E-mail")
        textView.text = "leonzawodowiec@gmail.com"
        textView.autocorrectionType = .no
        textView.backgroundColor = .white
        textView.keyboardType = UIKeyboardType.emailAddress
        textView.autocapitalizationType = .none
        textView.setLeftPaddingPoints(8)
        return textView
    }()
    
    var passwordField: UITextField = {
        let textView = UITextField()
        textView.placeholder = Language.getWord("Password")
        textView.text = "Qwerty123"
        textView.backgroundColor = .white
        textView.setLeftPaddingPoints(8)
        textView.isSecureTextEntry = true;
        return textView
    }()
    
    lazy var loginButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = Color.secondaryColor
        button.setTitle(Language.getWord("Sign In"), for: .normal)
        button.addTarget(self, action: #selector(handledLogin), for: .touchUpInside)
        return button
    }()
    
    var registerLabel: UILabel = {
        var label = UILabel()
        label.textColor = .white
        label.font = label.font.withSize(12)
        label.text = Language.getWord("Don`t have any account?")
        return label
    }()
    
    lazy var createOneLabel: UILabel = {
        var label = UILabel()
        label.textColor = Color.secondaryColor
        label.font = UIFont.boldSystemFont(ofSize: 12.0)
        label.attributedText = NSAttributedString(string: Language.getWord("Create One"), attributes: [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        var tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handledRegister))
        tapGestureRecognizer.numberOfTapsRequired = 1;
        label.addGestureRecognizer(tapGestureRecognizer)
        label.isUserInteractionEnabled = true;
        return label
    }()
    
    var separator: UIView = {
        var view = UIView()
        view.backgroundColor = .gray
        return view;
    }()
}
