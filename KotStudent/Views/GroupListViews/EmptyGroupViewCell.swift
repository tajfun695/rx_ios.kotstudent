//
//  EmptyGroupViewCell.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 09/08/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import UIKit

class EmptyGroupViewCell: BaseCell {
    let text: UILabel = UILabel()
    override func setupViews() {
        self.backgroundColor = Color.background
        self.setupBody()
        self.setupConstraints()
    }
    
    fileprivate func setupBody(){
        self.text.textColor = Color.cellColor
        self.text.text = "Nie jesteś jeszcze żadnej groupe!"
        
    }
    
    fileprivate func setupConstraints() {
        self.text.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
}
