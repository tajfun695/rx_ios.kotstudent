//
//  GroupView.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 14.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class GroupsView: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    //MARK: PrivateValue
    private let cellID: String = "GroupCellID"
    private let emptyCellID: String = "EmptyCellID"
    
    //MARK: PublicVariable
    var viewModel: GroupViewModel = GroupViewModel()
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.perform(#selector(loginToService))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.loadGroups()
        self.setupNavigator()
        self.setupBody()
    }
    
    fileprivate func setupNavigator() {
        let searchTap = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchTapped))
        
        navigationItem.title = "Groups"
        navigationItem.rightBarButtonItems = [searchTap]
        navigationController?.navigationBar.backgroundColor = Color.mainColor
    }
    
    fileprivate func setupBody() {
        collectionView?.backgroundColor = Color.lightGrey
        self.collectionView?.dataSource = nil
        self.collectionView?.register(GroupsViewCell.self, forCellWithReuseIdentifier: cellID)
        self.collectionView?.register(EmptyGroupViewCell.self, forCellWithReuseIdentifier: emptyCellID)
        
        self.viewModel.groups.asObservable()
        .bind(to: collectionView!.rx.items(cellIdentifier: cellID, cellType: GroupsViewCell.self))
        { index, item, cell in
            cell.data = item
        }.disposed(by: disposeBag)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 80)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //getting the current cell from the index path
        let currentCell = collectionView.cellForItem(at: indexPath) as! GroupsViewCell
        
        let layout = UICollectionViewFlowLayout()
        let groupView: SingielGroup = SingielGroup(collectionViewLayout: layout)
        groupView.viewModel = SingielGroupViewModel(groupID: currentCell.data!.id!)
        groupView.viewModel?.LoadGroup(completion: { isLoading in
            if isLoading == true {
                groupView.viewModel?.LoadPosts(completion: { isPostLoading in
                    if isPostLoading == true {
                        self.navigationController?.pushViewController(groupView, animated: true)
                    }
                })
            }
        })
    }
    
    @objc fileprivate func loginToService(){
        let loginView = LoginView()
        self.present(loginView, animated: true)
    }
    
    @objc fileprivate func searchTapped() {
        print("Tapped :)")
        let searchView = SearchView(collectionViewLayout: UICollectionViewFlowLayout())
        self.navigationController?.pushViewController(searchView, animated: false)
    }
}
