//
//  GroupsCellView.swift
//  KotStudent
//
//  Created by Bartłomiej Łaski on 15.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit
import SnapKit

class GroupsViewCell: BaseCell {
    
    override var isHighlighted: Bool {
        didSet {
            self.backgroundColor = isHighlighted ? Color.lightGrey : Color.cellColor
        }
    }
    
    var data: Group? {
        didSet{
            groupName.text = data?.name
        }
    }
    
    override func setupViews() {
        self.backgroundColor = Color.cellColor
        self.addSubview(groupName)
        self.addSubview(arrow)
        
        groupName.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.right.equalToSuperview().offset(12)
        }
        arrow.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-12)
            make.height.width.equalTo(24)
        }
    }
    
    let groupName: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18.0);
        return label
    }()
    
    let arrow: UIImageView = {
        let image = UIImageView(image: UIImage(named: "arrowRight"))
        return image
    }()
}
