//
//  NotificationView.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 14.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit

class NotificationView: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Notifications"
        collectionView?.backgroundColor = .red
    }
}
