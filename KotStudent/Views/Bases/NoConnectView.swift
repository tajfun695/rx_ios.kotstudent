//
//  NoConnectView.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 07/08/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import Foundation
import UIKit

class NoConnectView: UIView {
    
    let text = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .red
        self.addSubview(text)
        self.setText()
    }
    
    fileprivate func setText() {
        self.text.text = "No connect with the Internet"
        self.text.textColor = .white
        self.text.font.withSize(8)
        
        self.text.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.bottomMargin.equalTo(4)
            make.centerX.equalToSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
