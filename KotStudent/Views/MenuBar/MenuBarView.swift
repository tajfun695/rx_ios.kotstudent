//
//  MenuBarView.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 11.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit

class MenuBarView: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        viewControllers = [
            //createUICollectionViewController(title: "Dashboard", imageName: "home", controller: DashboardView(collectionViewLayout: UICollectionViewFlowLayout())),
            createUIViewController(title: "Group", imageName: "groupIcon", controller: GroupsView(collectionViewLayout: UICollectionViewFlowLayout())),
            //createUIViewController(title: "Notification", imageName: "notification", controller: GroupView()),
            createUIViewController(title: "Settings", imageName: "menuIcon", controller: SettingView(collectionViewLayout: UICollectionViewFlowLayout()))]
    }
    
    private func createUIViewController(title: String, imageName: String, controller: UIViewController) -> UINavigationController {
        let viewController = controller
        let navController = UINavigationController(rootViewController: viewController)
        navController.tabBarItem.title = title
        navController.tabBarItem.image = UIImage(named: imageName)
        return navController
    }
    
    private func createUICollectionViewController(title: String, imageName: String, controller: UICollectionViewController) -> UINavigationController{
        let viewController = controller
        let navController = UINavigationController(rootViewController: viewController)
        navController.tabBarItem.title = title
        navController.tabBarItem.image = UIImage(named: imageName)
        return navController
    }
}
