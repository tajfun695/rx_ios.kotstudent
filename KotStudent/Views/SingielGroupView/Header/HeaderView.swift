//
//  CollectionViewSectionHeader.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 25.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class HeaderView: UICollectionReusableView {
    var disposeBag = DisposeBag()
    
    public var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .gray
        return cv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        if let flow =  collectionView.collectionViewLayout as? UICollectionViewFlowLayout{
            flow.scrollDirection = .horizontal
            flow.sectionHeadersPinToVisibleBounds = true
        }
        collectionView.register(ThreadCellView.self, forCellWithReuseIdentifier: "cellId")
        collectionView.frame = self.bounds
        self.addSubview(collectionView)
    }
    
    func bind(viewModel: SingielGroupViewModel){
        viewModel.group.asObservable()
            .map{ $0.threads }
            .bind(to: collectionView.rx.items(cellIdentifier: "cellId", cellType: ThreadCellView.self)) {
                index, item, cell in
                cell.data = item
            }.disposed(by: disposeBag)
    }
}
