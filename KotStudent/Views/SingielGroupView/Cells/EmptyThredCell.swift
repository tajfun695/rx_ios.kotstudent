//
//  EmptyThredCell.swift
//  KotStudentApp
//
//  Created by Bartomiej Łaski on 13.05.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit

class EmptyThredCell: BaseCell {
    var data: String? {
        didSet{
            title.text = data
        }
    }
    
    override func setupViews(){
        self.backgroundColor = .white
        addSubview(title)
        title.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
    }
    
    var title: UILabel = {
        let text: UILabel = UILabel()
        text.text = "Brak postów w tym wątku."
        text.font = UIFont.systemFont(ofSize: 14.0);
        text.textColor = .black
        text.lineBreakMode = .byWordWrapping
        text.numberOfLines = 0
        return text
    }()
}
