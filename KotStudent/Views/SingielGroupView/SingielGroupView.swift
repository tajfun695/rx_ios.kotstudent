//
//  SingielGroupView.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 20/08/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import GSKStretchyHeaderView

enum GroupViewIds: String {
    case HeaderId = "HeaderID"
    case CellId = "CellID"
    case EmptyId = "EmptyID"
}

class SingielGroup: UICollectionViewController {
    private let disposeBag = DisposeBag()
    private var layout: UICollectionViewLayout? {
        return collectionView?.collectionViewLayout
    }

    var viewModel: SingielGroupViewModel?
    var stretchyHeader: CollectionParallaxHeader? 
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionView?.backgroundColor = Color.background
        self.collectionView?.alwaysBounceVertical = true
        self.loadLayoyt()
        
    }
    
    func loadLayoyt() {
        // --Mark-- // HEADER
        let headerSize = CGSize(width: self.collectionView!.frame.size.width, height: 240)
        self.stretchyHeader = CollectionParallaxHeader(frame: CGRect(x: 0, y: 0, width: headerSize.width, height: headerSize.height))
        self.stretchyHeader?.setupGroupInfo(groupViewModel: viewModel!)
        self.collectionView?.addSubview(self.stretchyHeader!)
        
        // --Mark-- // Setup Cell
        self.collectionView?.register(PostViewCell.self, forCellWithReuseIdentifier: GroupViewIds.CellId.rawValue)
        //self.collectionView?.register(HeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: GroupViewIds.HeaderId.rawValue)
        self.collectionView?.register(EmptyThredCell.self, forCellWithReuseIdentifier: GroupViewIds.EmptyId.rawValue)
        
        self.collectionView?.delegate = nil
        self.collectionView?.dataSource = nil
        viewModel?.dataSource.configureCell = { dataSource, cv, indexPath, item in
            let cell = cv.dequeueReusableCell(withReuseIdentifier: GroupViewIds.CellId.rawValue, for: indexPath) as! PostViewCell
            cell.data = item
            return cell
        }
        
        viewModel?.dataSource.configureSupplementaryView = { dataSource, cv, item, indexPath in
            let cell = cv.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: GroupViewIds.HeaderId.rawValue, for: indexPath) as! HeaderView
            cell.bind(viewModel: self.viewModel!)
            return cell
        }
        
        self.viewModel?.posts.asObservable()
            .map { [SectionViewModel(header: HeaderView(), items: $0)] }
            .bind(to: self.collectionView!.rx.items(dataSource: viewModel!.dataSource))
            .disposed(by: disposeBag)
    }
    
}

extension SingielGroup: UICollectionViewDelegateFlowLayout {
    //HEADER SIZE!
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 30)
    }
}

struct SectionViewModel {
    var header: HeaderView
    var items: [Post]
}

extension SectionViewModel: SectionModelType {
    typealias Item = Post
    
    init(original: SectionViewModel, items: [Item]) {
        self = original
        self.items = items
    }
}
