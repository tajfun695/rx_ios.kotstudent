//
//  Post.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 01.05.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import Foundation

public struct Post {
    var id: Int?
    var content: String?
    var createDate: Date?
    var modifyDate: Date?
    var commentsCount: Int?
    var creator: User?
    
    init(){
        
    }
    
    init(json: [String: Any]) throws {
        if let id = json["id"] as? Int {
            self.id = id
        }
        
        if let content = json["content"] as? String {
            self.content = content
        }
        
        if let createDate = json["createDate"] as? String {
            self.createDate = DataConverter.DateParse(date: createDate)
        }

        if let modifyDate = json["modifyDate"] as? String {
            self.modifyDate = DataConverter.DateParse(date: modifyDate)
        }
        
        if let commentsCount = json["commentsCount"] as? Int {
            self.commentsCount = commentsCount
        }

        if let creator = json["creator"] as? [String: Any] {
            self.creator = User(json: creator)
        }
    }
}

