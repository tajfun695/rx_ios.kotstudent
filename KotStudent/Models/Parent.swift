//
//  Parent.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 06/07/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import Foundation

public struct Parent: Decodable {
    var id: Int?
    var name: String?
    var type: String?
    var group_path: String?
    
    init(json: [String: Any]) {
        if let id = json["id"] as? Int {
            self.id = id
        }
        
        if let name = json["name"] as? String {
            self.name = name
        }
        
        if let type = json["type"] as? String {
            self.type = type
        }
        
        if let group_path = json["group_path"] as? String {
            self.group_path = group_path
        }
    }
}
