//
//  Status.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 04/07/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import Foundation

public struct Status: Decodable {
    var status: Bool
}
