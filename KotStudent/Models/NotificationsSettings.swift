//
//  NotificationsSettings.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 06/07/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import Foundation

public struct NotificationsSettings: Decodable {
    var new_post: Bool?
    var post_edited: Bool?
    var post_commented: Bool?
    var own_post_commented: Bool?
    var file_uploaded: Bool?
    var file_commented: Bool?
    var own_file_commented: Bool?
    
    init(json: [String: Bool]) {
        if let new_post = json["new_post"] {
            self.new_post = new_post
        }
        
        if let post_edited = json["post_edited"] {
            self.post_edited = post_edited
        }
        
        if let post_commented = json["post_commented"] {
            self.post_commented = post_commented
        }
        
        if let own_post_commented = json["own_post_commented"] {
            self.own_post_commented = own_post_commented
        }
        
        if let file_uploaded = json["file_uploaded"] {
            self.file_uploaded = file_uploaded
        }
        
        if let file_commented = json["file_commented"] {
            self.file_commented = file_commented
        }
        
        if let own_file_commented = json["own_file_commented"] {
            self.own_file_commented = own_file_commented
        }
    }
}
