//
//  Group.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 06/07/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import Foundation

public struct Group: Decodable {
    var id: Int?
    var name: String?
    var nameAliases: [String]?
    var description: String?
    var photoUrl: String?
    var coverUrl: String?
    var createDate: Date?
    var endDate: Date?
    var opened: Bool?
    var memberCounter: Int?
    var type: String?
    var creator: User?
    var group_path: String?
    var isObservates: Bool?
    var notificationsSettings: NotificationsSettings?
    var parent: Parent?
    
    init(json: [String: Any]) throws {
        if let id = json["id"] as? Int {
            self.id = id
        }
        
        if let name = json["name"] as? String {
            self.name = name
        }
        
        if let nameAliases = json["nameAliases"] as? [String] {
            self.nameAliases = nameAliases
        }
        
        if let description = json["description"] as? String {
            self.description = description
        }
        
        if let photoUrl = json["photoUrl"] as? String {
            self.photoUrl = photoUrl
        }
        
        if let coverUrl = json["coverUrl"] as? String {
            self.coverUrl = coverUrl
        }
        
        if let createDate = json["createDate"] as? String {
            self.createDate = DataConverter.DateParse(date: createDate)
        }
        
        if let endDate = json["endDate"] as? String {
            self.endDate = DataConverter.DateParse(date: endDate)
        }
        
        if let opened = json["opened"] as? Bool {
            self.opened = opened
        }
        
        if let memberCounter = json["memberCounter"] as? Int {
            self.memberCounter = memberCounter
        }
        
        if let type = json["type"] as? String?{
            self.type = type
        }
        
        if let creator = json["creator"] as? [String: Any] {
            self.creator = User(json: creator)
        }
        
        if let group_path = json["group_path"] as? String {
            self.group_path = group_path
        }
        
        if let isObservates = json["isObservates"] as? Bool {
            self.isObservates = isObservates
        }
        
        if let notificationsSettings = json["notificationsSettings"] as? [String: Bool] {
            self.notificationsSettings = NotificationsSettings(json: notificationsSettings)
        }
        
        if let parent = json["parent"] as? [String: Any] {
            self.parent = Parent(json: parent)
        }
    }
}
