//
//  BaseError.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 04/07/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import Foundation

public struct APIError: LocalizedError{
    var identifier: Int
    var description: String?
    
    public var errorDescription: String? {
        get {
            return self.description
        }
    }
    
    init(_identifier: Int, _description: String?) {
        self.identifier = _identifier
        self.description = _description
    }
}
