//
//  Thread.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 16/07/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import Foundation

public struct Thread {
    var id: Int?
    var title: String?
    var createDate: Date?
    var creator: User?
    
    init(json: [String: Any]) {
        if let id = json["id"] as? Int {
            self.id = id
        }
        
        if let title = json["title"] as? String {
            self.title = title
        }
        
        if let createDate = json["createDate"] as? String {
            self.createDate = DataConverter.DateParse(date: createDate)
        }
        
        if let creator = json["creator"] as? [String: Any] {
            self.creator = User(json: creator)
        }
    }
}
