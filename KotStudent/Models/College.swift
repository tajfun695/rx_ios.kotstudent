//
//  College.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 13/07/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import Foundation

class College {
    var id: Int?
    var name: Int?
    var nameAliases: [String]?
    var description: String?
    var photoUrl: String?
    var coverUrl: String?
    var createDate: Date?
    var endDate: Date?
    var opened: Bool?
    var creator: User?
    var group_path: String?
    
    
}

/*
 "id": 8,
 "name": "Uniwersytet Jagieloński",
 "nameAliases": [],
 "description": null,
 "photoUrl": null,
 "coverUrl": null,
 "createDate": "2017-04-03T12:45:33.000Z",
 "endDate": null,
 "opened": true,
 "creator": {
 "userId": 2,
 "userName": "Konrad Pawlik",
 "userEmailAddress": "hawkmedia24@gmail.com",
 "userRegisterDate": null,
 "userFirstName": "Konrad",
 "userMiddleName": null,
 "userLastName": "Pawlik",
 "userBirthdayDate": null,
 "userPhotoUrl": null,
 "userCoverUrl": null,
 "userGender": null
 },
 "group_path": "/uniwersytet-jagielonski"
 */
