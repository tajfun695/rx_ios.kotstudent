//
//  User.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 06/07/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import Foundation

public struct User: Decodable {
    var userId: Int?
    var userName: String?
    var userEmailAddress: String?
    var userRegisterDate: Date?
    var userFirstName: String?
    var userMiddleName: String?
    var userLastName: String?
    var userBirthdayDate: Date?
    var userPhotoUrl: String?
    var userCoverUrl: String?
    var userGender: String?
    var userOptions: UserOptions?
    var memberships: [Group]?
    
    init(json: [String: Any]) {
        if let userId = json["userId"] as? Int {
            self.userId = userId
        }
        
        if let userName = json["userName"] as? String {
            self.userName = userName
        }
        
        if let userEmailAddress = json["userEmailAddress"] as? String {
            self.userEmailAddress = userEmailAddress
        }
        
        if let userRegisterDate = json["userRegisterDate"] as? String {
            self.userRegisterDate = DataConverter.DateParse(date: userRegisterDate)
        }
        
        if let userFirstName = json["userFirstName"] as? String {
            self.userFirstName = userFirstName
        }
        
        if let userMiddleName = json["userMiddleName"] as? String {
            self.userMiddleName = userMiddleName
        }
        
        if let userLastName = json["userLastName"] as? String {
            self.userLastName = userLastName
        }
        
        if let userBirthdayDate = json["userBirthdayDate"] as? String {
            self.userBirthdayDate = DataConverter.DateParse(date: userBirthdayDate)
        }
        
        if let userPhotoUrl = json["userPhotoUrl"] as? String {
            self.userPhotoUrl = userPhotoUrl
        }
        
        if let userCoverUrl = json["userCoverUrl"] as? String {
            self.userCoverUrl = userCoverUrl
        }
        
        if let userGender = json["userGender"] as? String {
            self.userGender = userGender
        }
        
        if let userOptions = json["userGender"] as? [String: Bool]{
            self.userOptions = UserOptions(json: userOptions)
        }
        
//        if let memberships = json["memberships"] as? [String: Any] {
//            for item in memberships {
//                self.memberships?.append(Group(json: item))
//            }
//        }
    }
    
}

public struct UserOptions: Decodable {
    var showWelcomeScreen: Bool?
    var showEmailAddress: Bool?
    
    init(json: [String: Bool]){
        if let showWelcomeScreen = json["showWelcomeScreen"] {
            self.showWelcomeScreen = showWelcomeScreen
        }
        
        if let showEmailAddress = json["showEmailAddress"] {
            self.showEmailAddress = showEmailAddress
        }
    }
}
