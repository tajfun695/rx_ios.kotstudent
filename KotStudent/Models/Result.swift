//
//  Result.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 06/07/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import Foundation

public enum Result<T>{
    case onSuccess(T)
    case onFail(Error)
}
