//
//  ComplexGroup.swift
//  KotStudent
//
//  Created by Bartomiej Łaski on 16/07/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import Foundation

public struct ComplexGroup {
    var group: Group?
    var topUsers: [User] = []
    var threads: [Thread] = []
    
    init() {
        
    }
    
    init(group: [String: Any], topUsers: [Any], threads: [Any]) {
    
        self.group = try? Group(json: group)
        
        for user in topUsers {
            self.topUsers.append(User(json: user as! [String: Any]))
        }
        
        for thread in threads {
            self.threads.append(Thread(json: thread as! [String: Any]))
        }
    }
}
