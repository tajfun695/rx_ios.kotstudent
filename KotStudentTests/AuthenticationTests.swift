//
//  Authentication.swift
//  KotStudentTests
//
//  Created by Bartomiej Łaski on 04/07/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import XCTest
@testable import KotStudent

class AuthenticationTests: XCTestCase {
    
    func testLoginViaEmail_OnSucces() {
        let expectation = XCTestExpectation(description: "[TEST] Login via E-mail to serwis.")
        
        Authentication.LoginViaEmail(_username: "leonzawodowiec@gmail.com", _password: "Qwerty123") { (status) in
            switch status {
            case .onSuccess(let result):
                XCTAssertEqual(result.status, true)
                expectation.fulfill()
            case .onFail(let error):
                XCTAssertNil(error, "I should login!")
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testLoginViaEmail_BadPassword() {
        let expectation = XCTestExpectation(description: "[TEST] Login via E-mail. BadPassword")
        
        Authentication.LoginViaEmail(_username: "leonzawodowiec@gmail.com", _password: "Qwerty1233") { (status) in
            switch status {
            case .onSuccess(let result):
                XCTAssertNil(result.status, "Should be throw error!")
                expectation.fulfill()
            case .onFail(let error):
                XCTAssertEqual(error.localizedDescription, "Entered password is invalid")
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testLoginViaEmail_BadEmail() {
        let expectation = XCTestExpectation(description: "[TEST] Login via E-mail. BadMail")
        
        Authentication.LoginViaEmail(_username: "leonzawodowiec@gmail.com1", _password: "Qwerty123") { (status) in
            switch status {
            case .onSuccess(let result):
                XCTAssertNil(result.status, "Should be throw error!")
                expectation.fulfill()
            case .onFail(let error):
                XCTAssertEqual(error.localizedDescription, "Given email address is not a correct email address. Value is invalid")
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testLoginViaEmail_NonFoundUser() {
        let expectation = XCTestExpectation(description: "[TEST] Login via E-mail. NonFoundUser")
        
        Authentication.LoginViaEmail(_username: "leonzawodowiec1@gmail.com", _password: "Qwerty123") { (status) in
            switch status {
            case .onSuccess(let result):
                XCTAssertNil(result.status, "Should be throw error!")
                expectation.fulfill()
            case .onFail(let error):
                XCTAssertEqual(error.localizedDescription, "")
                expectation.fulfill()   
            }
        }
        
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testLoginViaFacebook() {
        let expectation = XCTestExpectation(description: "[TEST] Login via Facebook")
        let token = "EAAFhRvGZC1jEBAIfiBf9pQ1zHVdCZBb5T5riiQO689ZCi2dcl7MxIRUzlnUNe8XpUX6VWAorqYukigGAZBTnQa7M5XYRUUOivrTfgU2mLr4iMstopvs7cZCKNx5V0BucOSqMVdQqgkGPnuzsy2R9LjYjrj2KZBXA9gMIdLmO9XSwVGt952viOYiR5zTBfqd6pFnd6TNKUPl8lMQnGDZAlJvY70eTPQuc3ItWDNQAYU6TwZDZD"
        
        Authentication.LoginViaFacebook(_token: token) { (status) in
            switch status {
            case .onSuccess(let result):
                XCTAssertEqual(result.status, true)
                expectation.fulfill()
            case .onFail(let error):
                XCTAssertNil(error.localizedDescription, "Should be throw error!")
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testLogout_OnSuccess() {
        let expectation = XCTestExpectation(description: "[TEST] Logout")
        
        Authentication.LoginViaEmail(_username: "leonzawodowiec@gmail.com", _password: "Qwerty123") { (status) in
            switch status {
            case .onSuccess:
                Authentication.Logout { (status) in
                    switch status {
                    case .onSuccess(let result):
                        XCTAssertEqual(result.status, true)
                        expectation.fulfill()
                    case .onFail(let error):
                        XCTAssertNil(error.localizedDescription, "")
                    }
                }
                
            case .onFail(let error):
                XCTAssertNil(error.localizedDescription, "Login error")
            }
        }
        

        
        wait(for: [expectation], timeout: 10.0)
    }
}

