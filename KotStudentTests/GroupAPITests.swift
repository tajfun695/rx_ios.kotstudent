//
//  GroupAPITests.swift
//  KotStudentTests
//
//  Created by Bartomiej Łaski on 06/07/2018.
//  Copyright © 2018 Bartomiej Łaski. All rights reserved.
//

import XCTest
@testable import KotStudent

class GroupAPITests: XCTestCase {
    
    func testGroupChilds() {
        let expectation = XCTestExpectation(description: "[TEST] Group childs")
        
        Authentication.LoginViaEmail(_username: "leonzawodowiec@gmail.com", _password: "Qwerty123") { (status) in
            switch status {
            case .onSuccess:
                GroupAPI.GetGroupChilds(groupId: 1) { (status) in
                    switch status {
                    case .onSuccess(let groups):
                        XCTAssertEqual(groups.count, 6)
                        expectation.fulfill()
                    case .onFail(let error):
                        print(error.localizedDescription)
                        expectation.fulfill()
                    }
                }
            case .onFail(let error):
                XCTAssertNil(error, "I should login!")
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testFindCollege() {
        let expectation = XCTestExpectation(description: "[TEST] Find college")
        
        
        
        Authentication.LoginViaEmail(_username: "leonzawodowiec@gmail.com", _password: "Qwerty123") { (status) in
            switch status {
            case .onSuccess:
                GroupAPI.FindCollege(collegeNamePhrase: "uniw"){ (status) in
                    switch status {
                    case .onSuccess(let groups):
                        XCTAssertEqual(groups.count, 3)
                        expectation.fulfill()
                    case .onFail(let error):
                        XCTAssertNil(error)
                        expectation.fulfill()
                    }
                }
            case .onFail(let error):
                XCTAssertNil(error, "I should login!")
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testFindGroup() {
        let expectation = XCTestExpectation(description: "[TEST] Find college")
        
        Authentication.LoginViaEmail(_username: "leonzawodowiec@gmail.com", _password: "Qwerty123") { (status) in
            switch status {
            case .onSuccess:
                GroupAPI.FindGroups(searchPhrase: "inf"){ (status) in
                    switch status {
                    case .onSuccess(let groups):
                        XCTAssertEqual(groups.count, 7)
                        expectation.fulfill()
                    case .onFail(let error):
                        print(error.localizedDescription)
                        expectation.fulfill()
                    }
                }
            case .onFail(let error):
                XCTAssertNil(error, "I should login!")
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testComplexInformation() {
        let expectation = XCTestExpectation(description: "[TEST] Find college")
        
        Authentication.LoginViaEmail(_username: "leonzawodowiec@gmail.com", _password: "Qwerty123") { (status) in
            switch status {
            case .onSuccess:
                GroupAPI.GetCompleteInfo(groupId: 1){ (status) in
                    switch status {
                    case .onSuccess(let group):
                        XCTAssertEqual(group.group?.name, "Uniwersytet Łódzki")
                        XCTAssertEqual(group.topUsers.count, 0)
                        XCTAssertEqual(group.threads.count, 1)
                        expectation.fulfill()
                    case .onFail(let error):
                        print(error.localizedDescription)
                        XCTAssertNil(error)
                        expectation.fulfill()
                    }
                }
            case .onFail(let error):
                XCTAssertNil(error, "I should login!")
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    
}
